package ru.reboot.dto;

/**
 * Event state.
 */
public enum EventState {
    TODO,
    CANCELED,
    DONE
}