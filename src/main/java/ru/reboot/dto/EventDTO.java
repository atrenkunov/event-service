package ru.reboot.dto;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;


public class EventDTO {

    private String id;
    private String userId;
    private String name;
    private String description;
    private LocalDate date;
    private LocalTime time;
    private EventState state;
    private EventPriority priority;

    public static Builder builder() {
        return new EventDTO().new Builder();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public EventState getState() {
        return state;
    }

    public void setState(EventState state) {
        this.state = state;
    }

    public EventPriority getPriority() {
        return priority;
    }

    public void setPriority(EventPriority priority) {
        this.priority = priority;
    }

    public class Builder {

        private Builder() {
            // private constructor
        }

        public Builder setId(String id) {
            EventDTO.this.id = id;
            return this;
        }

        public Builder setUserId(String userId) {
            EventDTO.this.userId = userId;
            return this;
        }

        public Builder setName(String name) {
            EventDTO.this.name = name;
            return this;
        }

        public Builder setDescription(String description) {
            EventDTO.this.description = description;
            return this;
        }

        public Builder setDate(LocalDate date) {
            EventDTO.this.date = date;
            return this;
        }

        public Builder setTime(LocalTime time) {
            EventDTO.this.time = time;
            return this;
        }

        public Builder setState(EventState state) {
            EventDTO.this.state = state;
            return this;
        }

        public Builder setPriority(EventPriority priority) {
            EventDTO.this.priority = priority;
            return this;
        }

        public EventDTO build() {
            return EventDTO.this;
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EventDTO eventDTO = (EventDTO) o;
        return id.equals(eventDTO.id) && Objects.equals(userId, eventDTO.userId) && Objects.equals(name, eventDTO.name) && Objects.equals(description, eventDTO.description) && Objects.equals(date, eventDTO.date) && Objects.equals(time, eventDTO.time) && state == eventDTO.state && priority == eventDTO.priority;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userId, name, description, date, time, state, priority);
    }

    @Override
    public String toString() {
        return "EventDTO{" +
                "id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", date=" + date +
                ", time=" + time +
                ", state=" + state +
                ", priority=" + priority +
                '}';
    }
}
