package ru.reboot.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.reboot.dao.EventRepository;
import ru.reboot.dao.entity.EventEntity;
import ru.reboot.dto.EventDTO;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCodes;


import java.time.LocalDate;
import java.util.ArrayList;
import java.time.LocalTime;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class EventServiceImpl implements EventService {

    private static final Logger logger = LogManager.getLogger(EventServiceImpl.class);
    private EventRepository eventRepository;

    public static EventDTO getEventDTO(EventEntity eventEntity) {
        return EventDTO.builder()
                .setId(eventEntity.getId())
                .setUserId(eventEntity.getUserId())
                .setName(eventEntity.getName())
                .setDescription(eventEntity.getDescription())
                .setDate(Objects.isNull(eventEntity.getDate()) ? null : LocalDate.parse(eventEntity.getDate()))
                .setTime(Objects.isNull(eventEntity.getTime()) ? null : LocalTime.parse(eventEntity.getTime()))
                .setState(eventEntity.getState())
                .setPriority(eventEntity.getPriority())
                .build();
    }

    public static EventEntity getEventEntity(EventDTO eventDTO) {
        EventEntity eventEntity = new EventEntity();

        eventEntity.setId(eventDTO.getId());
        eventEntity.setUserId(eventDTO.getUserId());
        eventEntity.setName(eventDTO.getName());
        eventEntity.setDescription(eventDTO.getDescription());
        eventEntity.setDate(Objects.isNull(eventDTO.getDate()) ? null : eventDTO.getDate().toString());
        eventEntity.setTime(Objects.isNull(eventDTO.getTime()) ? null : eventDTO.getTime().toString());
        eventEntity.setState(eventDTO.getState());
        eventEntity.setPriority(eventDTO.getPriority());

        return eventEntity;
    }

    @Autowired
    public void setEventRepository(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    /**
     * Получить список всех событий пользователя
     *
     * @param userId - user id
     */
    @Override
    public List<EventDTO> getAllUserEvents(String userId) {
        logger.info("Method .getAllUserEvents userId={}", userId);

        if (userId == null) {
            String errorCode = ErrorCodes.ILLEGAL_ARGUMENT.toString();
            RuntimeException ex = new BusinessLogicException(errorCode, errorCode);
            String logMessage = "Failed to .getAllUserEvents error={}" + ex;
            logger.error(logMessage, ex);
            throw ex;
        }

        List<EventEntity> eventEntityList = eventRepository.getAllUserEvents(userId);

        List<EventDTO> result = eventEntityList.stream()
                .map(EventServiceImpl::getEventDTO)
                .collect(Collectors.toList());

        logger.info("Method .getAllUserEvents userId={} , result={}", userId, result);
        return result;
    }

    /**
     * Получить список всех событий пользователя за определенную дату
     *
     * @param userId - user id
     */
    @Override
    public List<EventDTO> getUserEventsByDate(String userId, String date) {
        logger.info("Method .getAllUserEvents userId={} , date={}", userId, date);

        if (userId == null || date == null) {
            String errorCode = ErrorCodes.ILLEGAL_ARGUMENT.toString();
            RuntimeException ex = new BusinessLogicException(errorCode, errorCode);
            String logMessage = "Failed to .getUserEventsByDate error={}" + ex;
            logger.error(logMessage, ex);
            throw ex;
        }

        List<EventEntity> eventEntityList = eventRepository.getUserEventsByDate(userId, date);
        List<EventDTO> result = eventEntityList.stream()
                .map(EventServiceImpl::getEventDTO)
                .collect(Collectors.toList());

        logger.info("Method .getUserEventsByDate userId={} , date={} , result={}", userId, date, result);
        return result;
    }

    /**
     * Returns Event from database with specified eventId
     *
     * @param eventId
     * @return EventDTO
     */
    @Override
    public EventDTO getEvent(String eventId) {
        logger.info("method .getEvent invoked eventId={}",eventId);
        EventEntity entity = null;
        try {
            if (Objects.isNull(eventId)) {
                throw new BusinessLogicException("eventId is null", ErrorCodes.ILLEGAL_ARGUMENT.toString());
            }
            entity = eventRepository.getEvent(eventId);
            if (Objects.isNull(entity)) {
                throw new BusinessLogicException("event not found", ErrorCodes.EVENT_NOT_FOUND.toString());
            }
            logger.info("method .getEvent completed, eventId={} find", eventId);
        } catch (Exception ex) {
            String message = "Failed to .getEvent error={}" + ex.toString();
            logger.error(message, ex);
            throw new BusinessLogicException(message, ex, getErrorCode(ex, ErrorCodes.GET_EVENT_BY_EVENT_ID_ERROR));
        }
        return getEventDTO(entity);
    }

    /**
     * Method delete event by eventId
     *
     * @param eventId
     * @return EventDTO
     */
    @Override
    public EventDTO deleteEvent(String eventId) {
        logger.info("method .deleteEvent invoked eventId={}",eventId);
        EventEntity entity = null;
        try {
            if (Objects.isNull(eventId)) {
                throw new BusinessLogicException("eventId is null", ErrorCodes.ILLEGAL_ARGUMENT.toString());
            }
            entity = eventRepository.getEvent(eventId);
            if (Objects.isNull(entity)) {
                throw new BusinessLogicException("event not found", ErrorCodes.EVENT_NOT_FOUND.toString());
            } else {
                entity = eventRepository.deleteEvent(eventId);
                logger.info("method .deleteEvent completed, eventId={} received", eventId);
            }
        } catch (Exception ex) {
            String message = "Failed to .deleteEvent error={}" + ex.toString();
            logger.error(message, ex);
            throw new BusinessLogicException(message, ex, getErrorCode(ex, ErrorCodes.DELETE_EVENT_BY_EVENT_ID_ERROR));
        }
        return getEventDTO(entity);
    }

    /**
     * Method CANCELED event by eventId
     *
     * @param eventId
     * @return EventDTO
     */
    @Override
    public EventDTO cancelEvent(String eventId) {
        logger.info("method .cancelEvent invoked eventId={}",eventId);
        EventEntity entity = null;
        try {
            if (Objects.isNull(eventId)) {
                throw new BusinessLogicException("eventId is null", ErrorCodes.ILLEGAL_ARGUMENT.toString());
            }
            entity = eventRepository.getEvent(eventId);
            if (Objects.isNull(entity)) {
                throw new BusinessLogicException("event not found", ErrorCodes.EVENT_NOT_FOUND.toString());
            }else {
                entity = eventRepository.cancelEvent(eventId);
                logger.info("method .cancelEvent completed, eventId={} CANCELED", eventId);
            }
        } catch (Exception ex) {
            String message = "Failed to .cancelEvent error={}" + ex.toString();
            logger.error(message, ex);
            throw new BusinessLogicException(message, ex, getErrorCode(ex, ErrorCodes.CANCEL_EVENT_BY_EVENT_ID_ERROR));
        }
        return getEventDTO(entity);
    }

    /**
     * The method ends event by eventId
     *
     * @param eventId - event id
     * @return event
     */
    @Override
    public EventDTO finishEvent(String eventId) {
        logger.info("Method EventService.finishEvent eventId={}", eventId);
        EventEntity eventEntity = eventRepository.finishEvent(eventId);
        if (eventEntity == null) {
            logger.error("Event not found");
            return null;
        }
        EventDTO eventDTO = getEventDTO(eventEntity);
        logger.info("Method EventRepository.finishEvent competed eventId={}", eventId);
        return eventDTO;
    }

    @Override
    public EventDTO createEvent(EventDTO event) {
        logger.info("method .createEvent event={}", event);

        try {
            if (Objects.isNull(event)) {
                throw new BusinessLogicException("Event is null", ErrorCodes.ILLEGAL_ARGUMENT.toString());
            }
            setEventId(event);
            if (eventRepository.getEvent(event.getId()) != null) {
                throw new BusinessLogicException("Event already exists", ErrorCodes.EVENT_ALREADY_EXISTS.toString());
            }
            eventRepository.createEvent(getEventEntity(event));
            logger.info("Method .createEvent completed, event was created={}", event);
            return event;
        }
        catch (Exception ex) {
            String message = "Failed to .createEvent error={}" + ex.toString();
            logger.error(message, ex);
            throw new BusinessLogicException(message, ex, getErrorCode(ex, ErrorCodes.CREATE_EVENT_ERROR));
        }
    }

    /**
     * Service method for updating an event
     *
     * @param event - event to be updated in the database
     * @return The event succeeded on update or threw BusinessLogicException.class
     */
    @Override
    public EventDTO updateEvent(EventDTO event) {
        logger.info("method .updateEvent invoked event={}", event);
        return getEventDTO(eventRepository.updateEvent(getEventEntity(event)));
    }

    @Override
    public void deleteUserEventsByDate(String userId, String date) {
        logger.info("method .deleteUserEventsByDate userId={}, date={}", userId, date);

        try {
            if (Objects.isNull(userId) || Objects.isNull(date)) {
                throw new BusinessLogicException("userId or date is null", ErrorCodes.ILLEGAL_ARGUMENT.toString());
            }
            eventRepository.deleteUserEventsByDate(userId, date);
            logger.info("Method .deleteUserEventsByDate successfully completed");
        }
        catch (Exception ex) {
            String message = "Failed to .deleteUserEventsByDate error={}" + ex.toString();
            logger.error(message, ex);
            throw new BusinessLogicException(message, ex, getErrorCode(ex, ErrorCodes.DELETE_USER_EVENTS_BY_DATE));
        }
    }

    /**
     * Service method for adding a list of users to the database
     *
     * @param userId - user ID
     * @param date - date of creation of the list of events
     * @param events - list of events
     * @return List<EventDTO>
     */
    @Override
    public List<EventDTO> saveAllEvents(String userId, String date, List<EventDTO> events) {
        try {
            logger.info("method .deleteUserEventsByDate invoked userId={} date={}", userId, date);
            deleteUserEventsByDate(userId, date);
            logger.info("method .saveAllEvents invoked userId={} date={} events={}", userId, date, events);
            List<EventEntity> entityList = new ArrayList<>();
            for (EventDTO e : events) {
                e.setUserId(userId);
                e.setDate(LocalDate.parse(date));
                entityList.add(getEventEntity(e));
            }

            List<EventEntity> eventEntities = eventRepository.saveAllEvents(userId, date, entityList);

            List<EventDTO> dtoList = new ArrayList<>();
            for (EventEntity e : eventEntities) {
                dtoList.add(getEventDTO(e));
            }
            logger.info("method .saveAllEvents completed");
            return dtoList;
        } catch (Exception e) {
            logger.error("Failed to .saveAllEvents error={}", e.toString());
            throw new BusinessLogicException("Service method saveAllEvents error", e, ErrorCodes.SAVE_ALL_EVENTS_ERROR.toString());
        }
    }

    @Override
    public List<EventDTO> getNearestEvents(int delta) {
        logger.info("Method EventService.getNearestEvents started delta={}", delta);

        if (delta < 0) {
            logger.error("getNearestEvents error: illegal argument delta={}", delta);
            throw new BusinessLogicException("Error in getNearestEvents method", ErrorCodes.ILLEGAL_ARGUMENT.toString());
        }

        List<EventEntity> events = eventRepository.getNearestEvents(delta);
        List<EventDTO> eventDTOList = new ArrayList<>();

        if (events == null || events.isEmpty()) {
            logger.error("No nearest events");
        } else {
            for (EventEntity e : events) {
                eventDTOList.add(getEventDTO(e));
            }
            logger.info("Method EventService.getNearestEvents finished delta={}", delta);
        }
        return eventDTOList;
    }


    private void setEventId(EventDTO eventDTO) {
        String id = eventDTO.getId() == null ? UUID.randomUUID().toString() : eventDTO.getId();
        eventDTO.setId(id);
    }

    private String getErrorCode(Exception ex, ErrorCodes defaultCode) {
        if (ex.getClass().equals(BusinessLogicException.class))
            return ((BusinessLogicException) ex).getCode();
        else
            return defaultCode.name();
    }

    @Override
    public String toString() {
        return "EventServiceImpl{}";
    }
}
