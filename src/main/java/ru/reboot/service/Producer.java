package ru.reboot.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.reboot.dto.EventDTO;

import java.util.List;

@EnableScheduling
@Service
public class Producer {
    private static final Logger logger = LoggerFactory.getLogger(Producer.class);
    private static final String TOPIC = "EVENT_NOTIFICATION";
    private EventService eventService;

    @Autowired
    private KafkaTemplate<String, EventDTO> kafkaTemplate;

    @Autowired
    public void setEventService (EventService eventService){
        this.eventService = eventService;
    }

    @Scheduled(fixedRate = 300000)
    public void sendMessage(){
 
        List<EventDTO> eventDTOList = eventService.getNearestEvents(15);
        if(!eventDTOList.isEmpty()){
            for(EventDTO eventDTO: eventDTOList){
                kafkaTemplate.send(TOPIC, eventDTO);
                logger.info("#### >>> Producer.sendMessage >>> {}", eventDTO);
            }
        }
    }
}
