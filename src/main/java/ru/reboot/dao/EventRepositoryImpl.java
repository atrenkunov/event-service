package ru.reboot.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.reboot.dao.entity.EventEntity;
import ru.reboot.error.BusinessLogicException;
import ru.reboot.error.ErrorCodes;
import ru.reboot.dto.EventState;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Event repository.
 */
@Component
public class EventRepositoryImpl implements EventRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    /**
     * Получить список всех событий пользователя
     *
     * @param userId - user id
     */
    @Override
    public List<EventEntity> getAllUserEvents(String userId) {
        try (Session session = sessionFactory.getCurrentSession()) {
            session.beginTransaction();

            List<EventEntity> list = session.createQuery(
                    "FROM EventEntity WHERE userId = :tUserId")
                    .setParameter("tUserId", userId)
                    .list();
            return list;
        }
    }


    /**
     * Получить список всех событий пользователя за определенную дату
     *
     * @param userId - user id
     * @param date   - date
     */
    @Override
    public List<EventEntity> getUserEventsByDate(String userId, String date) {
        try (Session session = sessionFactory.getCurrentSession()) {
            session.beginTransaction();

            return session.createQuery(
                            "FROM EventEntity WHERE userId = :tUserId AND date = :tDate")
                    .setParameter("tUserId", userId)
                    .setParameter("tDate", date)
                    .list();
        }
    }

    @Override
    public EventEntity getEvent(String eventId) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(EventEntity.class, eventId);
        }
    }

    @Override
    public EventEntity deleteEvent(String eventId) {
        EventEntity eventEntity;
        Transaction tx = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            eventEntity = session.load(EventEntity.class, eventId);
            session.delete(eventEntity);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            throw new RuntimeException(e);
        }

        return eventEntity;
    }

    @Override
    public EventEntity cancelEvent(String eventId) {
        EventEntity eventEntity;
        Transaction tx = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            eventEntity = session.load(EventEntity.class, eventId);
            eventEntity.setState(EventState.CANCELED);
            session.save(eventEntity);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            throw new RuntimeException(e);
        }
        return eventEntity;
    }

    /**
     * DAO method ends event
     *
     * @param eventId - event id
     * @return event if success or throw exception BusinessLogicException.class
     */
    @Override
    public EventEntity finishEvent(String eventId) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            EventEntity eventEntity = session.get(EventEntity.class, eventId);
            eventEntity.setState(EventState.DONE);
            session.update(eventEntity);
            transaction.commit();
            return eventEntity;
        } catch (Exception ex) {
            throw new BusinessLogicException("FinishEvent failed", ex, ErrorCodes.EVENT_NOT_FOUND.name());
        }
    }

    @Override
    public EventEntity createEvent(EventEntity event) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(event);
            transaction.commit();
            return event;
        } catch (Exception e) {
            if (Objects.nonNull(transaction)) {
                transaction.rollback();
            }
            throw new RuntimeException(e);
        }
    }

    /**
     * DAO method of updating event data
     *
     * @param event - event to be updated in the database
     * @return event if success or throw exception BusinessLogicException.class
     */
    @Override
    public EventEntity updateEvent(EventEntity event) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(event);
            transaction.commit();
            return event;
        } catch (Exception e) {
            throw new BusinessLogicException("updateEvent method error", e, ErrorCodes.EVENT_NOT_FOUND.toString());
        }
    }

    @Override
    public void deleteUserEventsByDate(String userId, String date) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            Query query = session.createQuery("delete EventEntity where user_id = :userId and date = :date");
            query.setParameter("userId", userId);
            query.setParameter("date", date);
            query.executeUpdate();
            transaction.commit();
        } catch (Exception e) {
            if (Objects.nonNull(transaction)) {
                transaction.rollback();
            }
            throw new RuntimeException(e);
        }
    }

    /**
     * DAO method to update the list of event data
     *
     * @param userId - user ID
     * @param date - date of creation of the list of events
     * @param events - list of events
     * @return List<EventDTO>
     */
    @Override
    public List<EventEntity> saveAllEvents(String userId, String date, List<EventEntity> events) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            for (EventEntity e : events) {
                session.save(e);
            }
            transaction.commit();
            return events;
        } catch (Exception e) {
            throw new BusinessLogicException("saveAllEvents method error", e, ErrorCodes.SAVE_ALL_EVENTS_ERROR.toString());
        }
    }

    @Override
    public List<EventEntity> getNearestEvents(int delta) {
        LocalDateTime dateTimeEventsFrom = LocalDateTime.now().plusMinutes((long) delta);
        LocalDateTime dateTimeEventsTo = LocalDateTime.now().plusMinutes((long) delta + 5);
        LocalDate dateEventsFrom = dateTimeEventsFrom.toLocalDate();
        LocalDate dateEventsTo = dateTimeEventsTo.toLocalDate();
        LocalTime timeEventsFrom = dateTimeEventsFrom.toLocalTime();
        LocalTime timeEventsTo = dateTimeEventsTo.toLocalTime();

        String query = "from EventEntity where date >= :dateEventsFrom and date <= :dateEventsTo and state = 'TODO'";
        try (Session session = sessionFactory.openSession()) {
            List<EventEntity> eventsList = session
                    .createQuery(query)
                    .setParameter("dateEventsFrom", dateEventsFrom.toString())
                    .setParameter("dateEventsTo", dateEventsTo.toString())
                    .getResultList();
            List<EventEntity> events = new ArrayList<>();
            if(eventsList.isEmpty() || eventsList == null){
                return events;
            }
            for (EventEntity e : eventsList) {
                if(e.getTime() == null){
                } else if (dateEventsFrom.isEqual(dateEventsTo)){
                    if(LocalTime.parse(e.getTime()).equals(timeEventsFrom)){
                        events.add(e);
                    } else if(LocalTime.parse(e.getTime()).isAfter(timeEventsFrom)
                            && LocalTime.parse(e.getTime()).isBefore(timeEventsTo)){
                        events.add(e);
                    }
                } else {
                    if (LocalDate.parse(e.getDate()).equals(dateEventsFrom)){
                        if (LocalTime.parse(e.getTime()).equals(timeEventsFrom) ||
                            LocalTime.parse(e.getTime()).isAfter(timeEventsFrom)){
                            events.add(e);
                        }
                    } else {
                        if (LocalTime.parse(e.getTime()).isAfter(timeEventsTo)){
                            events.add(e);
                        }
                    }
                }
            }
            return events;
        } catch (Exception ex) {
            throw new BusinessLogicException("Error in EventRepository.getNearestEvents method", ErrorCodes.ILLEGAL_ARGUMENT.toString());
        }
    }

    @Override
    public String toString() {
        return "EventRepositoryImpl{}";
    }
}
