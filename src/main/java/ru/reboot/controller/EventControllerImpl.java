package ru.reboot.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.reboot.dto.EventDTO;
import ru.reboot.service.EventService;
import ru.reboot.service.EventServiceImpl;

import java.util.Date;
import java.util.List;

/**
 * Event controller.
 */
@RestController
@RequestMapping(path = "event")
public class EventControllerImpl implements EventController {

    private static final Logger logger = LogManager.getLogger(EventControllerImpl.class);

    private EventService eventService;

    @Autowired
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }

    @GetMapping("info")
    public String info() {
        logger.info("method .info invoked");
        return "EventController " + new Date();
    }


    /**
     * Получить список всех событий пользователя
     *
     * @param userId - user id
     */
    @Override
    @GetMapping("all/byUserId")
    public List<EventDTO> getAllUserEvents(@RequestParam("userId") String userId) {
        return eventService.getAllUserEvents(userId);
    }

    /**
     * Получить список всех событий пользователя за определенную дату
     *
     * @param userId - user id
     * @param date - date
     */
    @Override
    @GetMapping("all/byDate")
    public List<EventDTO> getUserEventsByDate(@RequestParam("userId") String userId,
                                              @RequestParam("date") String date) {
        return eventService.getUserEventsByDate(userId , date);
    }

    @Override
    @GetMapping("{eventId}")
    public EventDTO getEvent(@PathVariable("eventId") String eventId) {
        logger.info("method .getEvent invoked eventId={}",eventId);
        return eventService.getEvent(eventId);
    }

    @Override
    @DeleteMapping("{eventId}")
    public EventDTO deleteEvent(@PathVariable("eventId") String eventId) {
        logger.info("method .deleteEvent invoked eventId={}",eventId);
        return eventService.deleteEvent(eventId);
    }

    @Override
    @PutMapping("{eventId}/cancel")
    public EventDTO cancelEvent(@PathVariable("eventId") String eventId) {
        logger.info("method .cancelEvent invoked eventId={}",eventId);
        return eventService.cancelEvent(eventId);
    }

    /**
     * The method ends the user event
     *
     * @param eventId - event id
     * @return event
     */
    @Override
    @PutMapping("{eventId}/finish")
    public EventDTO finishEvent(@PathVariable("eventId") String eventId) {
        logger.info("Method .finishEvent started eventId={}", eventId);
        EventDTO eventDTO = eventService.finishEvent(eventId);
        return eventDTO;
    }

    @Override
    @PostMapping
    public EventDTO createEvent(@RequestBody EventDTO event) {
        return eventService.createEvent(event);
    }

    /**
     *The method updates the event in the database and returns the updated event.
     *
     * @param event - event to be updated in the database
     * @return event
     */
    @Override
    @PutMapping
    public EventDTO updateEvent(@RequestBody EventDTO event) {
        logger.info("method .updateEvent invoked event={}", event);
        return eventService.updateEvent(event);
    }

    @Override
    @DeleteMapping("all/byDate")
    public void deleteUserEventsByDate(@RequestParam("userId") String userId,
                                       @RequestParam("date") String date) {
        eventService.deleteUserEventsByDate(userId, date);
    }

    /**
     * Method that adds the list of events to the database
     *
     * @param userId - user ID
     * @param date - date of creation of the list of events
     * @param events - list of events
     * @return List<EventDTO>
     */
    @Override
    @PutMapping("all/byDate")
    public List<EventDTO> saveAllEvents(@RequestParam("userId") String userId,
                                        @RequestParam("date") String date,
                                        @RequestBody List<EventDTO> events) {
        logger.info("method .saveAllEvents invoked");
        return eventService.saveAllEvents(userId, date, events);
    }

    @Override
    @GetMapping("nearest")
    public List<EventDTO> getNearestEvents(@RequestParam("delta") int delta) {
        logger.info("Method EventController.getNearestEvents delta={}", delta);
        List <EventDTO> eventList = eventService.getNearestEvents(delta);
        logger.info("Method EventController.getNearestEvents completed delta={}", delta);
        return eventList;
    }
}
