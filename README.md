# Сервис event-service

Данный сервис хранит записи пользователей в дневнике.

Ответственный за сервис: Семенов Илья

## План работ:

#### Создать таблицу user_events. Написать liquibase скрипт. Данная таблица хранит записи пользователей в дневнике

Поля таблицы EventEntity

  * id: string
  * user_id: string
  * name: string
  * description: string
  * date: LocalDate
  * time: LocalTime
  * state: string
  * priority: string

### Создать соответствующий класс Event.

### Реализовать rest-контроллер /event/**/*, который обеспечивает работу с записями пользователя

План работ:

Илья Семенов
- Написать ликубейс скрипты и разместить их в папке sql, написать структуру EventEntity, EventDTO, getters & setters + builder
- Получить список всех событий пользователя GET /event/all/byUserId?userId={userId}
- Получить список всех событий пользователя за дату GET /event/all/byDate?userId={userId}&date={date}

Александр Жуков
- Получить информацию о конкретном событии GET /event/{eventId}
- Удалить информацию о событии DELETE /event/{eventId}
- Отменить событие PUT /event/{eventId}/cancel

Задубинин Игорь
- Создать новое событие пользователя POST /event
- Удалить все события пользователя DELETE /event/all/byDate?userId={userId}&date={date}

Денис Вузов
- Сохранить список событий пользователя PUT /event/all/byDate
Body: List<EventDTO>
- Редактировать существующее событие PUT /event

Ринат Нагуманов
- Получить информацию о предстоящих событиях всех пользователей /event/nearest?delta={delta}
(Если delta = 1 час то метод вернет предстоящие события, время до которых < delta)
- Завершить событие PUT /event/{eventId}/finish

Критерии приемки:
- методы сервиса должны быть протестированы.